﻿var app = angular.module('SIT.directives', [])

//=====================================================================================================================
// HIERARCHY CHOOSER DIRECTIVE
// USE: <hierarchy-chooser></hierarchy-chooser>
//=====================================================================================================================
app.directive('hierarchyChooser', function () {
    return {
        restrict: 'E',
        scope: {
            hierarchy: '=data', // two-way-bind from attribute named 'data'
            chosen: '=', // two-way-bind from attribute named 'chosen'
            title: '@',
            callback: '&', // function to perform when a choice is made
            any: '@'
        },
        controller: ['$scope', 'HierarchyElement', function ($scope, HierarchyElement) {
            $scope.curData = $scope.hierarchy;
            $scope.history = [];
            $scope.choiceMade = false;

            $scope.chosenStyle = {
                "background-color": "rgb(230, 255, 255)"
            }
            $scope.notChosenStyle = {
                "background-color" : "white"
            }

            $scope.goDown = function (data) {
                $scope.history.push($scope.curData);
                $scope.curData = data;
            }

            $scope.goUp = function () {
                if ($scope.history.length > 0) {
                    var data = $scope.history.pop();
                    $scope.curData = data;
                }
            }
            
            var setChosen = function (choice) {
                $scope.chosen = choice;
                $scope.choiceMade = true;
                if ($scope.callback)
                    $scope.callback({ element: choice });
            }

            var resetChosen = function () {
                $scope.chosen = null;
                $scope.choiceMade = false;
            }
            
            $scope.toggleChosen = function (choice) {
                var noChoiceMade = !$scope.choiceMade;
                var choosingOther = $scope.chosen === null || $scope.chosen.node != choice.node;
                var canChoose = noChoiceMade || choosingOther;
                if (canChoose) {
                    setChosen(choice);
                } else {
                    resetChosen();
                }
            }

            $scope.toggleChosenAny = function (choice) {
                var any = HierarchyElement.createAny();
                $scope.toggleChosen(any);
            }
        }],
        templateUrl: 'templates/directives/hierarchyChooser.html'
    };
});

app.directive('imageonload', function () {
    return {
        restrict: 'A',
        link: function (scope, element) {
            element.on('load', function () {
                // Set visibility: true + remove spinner overlay
                element.removeClass('spinner-hide');
                element.addClass('spinner-show');
                element.parent().find('span').remove();
            });
            scope.$watch('ngSrc', function () {
                // Set visibility: false + inject temporary spinner overlay
                element.addClass('spinner-hide');
                // element.parent().append('<span class="spinner"></span>');
            });
        }
    };
});

// http://ionicinaction.com/blog/ionic-custom-double-subheaders/
// used to allow multiple subheaders

// http://stackoverflow.com/questions/17772260/textarea-auto-height
// used to size textareas based on content length
app.directive('elastic', [
    '$timeout',
    function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, element) {
                $scope.initialHeight = $scope.initialHeight || element[0].style.height;
                var resize = function() {
                    element[0].style.height = $scope.initialHeight;
                    element[0].style.height = "" + element[0].scrollHeight + "px";
                };
                element.on("input change", resize);
                $timeout(resize, 0);
            }
        };
    }
]);