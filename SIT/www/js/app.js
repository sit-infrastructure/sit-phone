// SIT App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'SIT' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'SIT.services' is found in services.js
// 'SIT.controllers' is found in controllers.js
angular.module('SIT', ['ionic', 'SIT.controllers', 'SIT.services', 'SIT.directives', 'ion-gallery', 'ion-sticky', 'rzModule', 'ionic-datepicker', 'ngCordova'])
    //, 'ionic-native-transitions' DISABLED FOR EMULATION

.run(function($ionicPlatform, $rootScope) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleLightContent();
        }
    });

    $rootScope.endpoint = "http://54.152.248.165:5498";
})

.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
    $stateProvider
    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'MenuCtrl'
    })
    .state('app.defects', {
        url: '/defects',
        templateUrl: 'templates/defects.html',
        controller: 'InspectionCtrl'
    })
    .state('app.addDefect', {
        url: '/addDefect',
        templateUrl: 'templates/addDefect.html',
        controller: 'AddDefectCtrl',
        params: {
            'inspection': null,
            'defectTypes': null,
            'structuralElements': null,
            'locationDescriptions': null
        }
    })
    .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
    })

  // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('login');

});
