var app = angular.module("SIT.controllers", [])

//=====================================================================================================================
// MENU CONTROLLER
//=====================================================================================================================

app.controller("MenuCtrl", function ($scope, $http) {
});

//=====================================================================================================================
// LOGIN CONTROLLER
//=====================================================================================================================

app.controller("LoginCtrl", function ($scope, $rootScope, $state, Server, Inspector, Util) {
    $scope.data = {
        username: "jeff",
        password: "test"
    };
    
    $rootScope.data = {
    }

    $scope.userInputIsValid = function () {
        var usernameValid = $scope.data.username.length > 0;
        var passwordValid = $scope.data.password.length > 0; // might want to impose password rules?
        return usernameValid && passwordValid;
    }

    $scope.login = function () {
        Server.login($scope.data.username, $scope.data.password).then(function(response) {
            var first = response.firstname;
            var last = response.lastname;
            var inspector = response.inspector_id;

            $rootScope.data.user = Inspector.createNew(first, last, inspector);
            $rootScope.data.sessionKey = response.session_key;

            $state.go("app.defects");
        }, function (response) {
            Util.showErrorMessage("The application has encountered an error while logging in. Please check your network connectivity. Please ensure that the username and password you have provided are valid.");
        });
    }
});

//=====================================================================================================================
// INSPECTION CONTROLLER
//=====================================================================================================================

app.controller("InspectionCtrl", function ($scope,
                                            $rootScope,
                                            $state,
                                            $ionicScrollDelegate,
                                            $location,
                                            $ionicLoading,
                                            $ionicModal,
                                            $ionicPopup,
                                            $ionicPopover,
                                            $timeout,
                                            $http,
                                            Server,
                                            HierarchyElement,
                                            Priority,
                                            Util,
                                            ImagePreloader,
                                            Distance,
                                            Geolocation,
                                            Sorting) {

    // init
    //==========================================================================
    $scope.priority = Priority;
    $scope.data = {
        defects: [],
        showFilters: false,
        filters: {
            filtersLoaded: false,

            showSortByChoices: false,
            showSortDirectionChoices: false,
            showStructureChoices: false,
            showInspectionChoices: false,
            showInspectorChoices: false,
            showCompletionChoices: false,

            sortByValues: [
                Sorting.Inspector(),
                Sorting.Date(),
                Sorting.Distance(),
                Sorting.Priority()
            ],
            completionValues: [
                "Saved",
                "Drafts",
                "Saved and Drafts"
            ],
            filterHierarchy: [],

            sortByChoice: Sorting.Date(),
            structureChoice: {},
            inspectionChoice: {},
            inspectorChoice: {},
            defectTypeChoice: HierarchyElement.createAny(),
            structuralElementChoice: HierarchyElement.createAny(),
            locationDescriptionChoice: HierarchyElement.createAny(),
            distanceChoice: 0,
            priorityChoice: 0,
            descriptionChoice: "",
            completionChoice: "Saved and Drafts"
        },
        imageModalData: {
            image: null
        },
        hierarchyChooserModalData: {
            hierarchy: {},
            chosen: {},
            title: "",
            onChoice: null
        },
        defectPopoverData: {
            deletedDefectNode: ""
        },
        serverData: {
            imageAddress: Server.getImageAddress()
        }
    }

    $scope.datePickerObject = {
        todayLabel: 'Today',  //Optional
        closeLabel: 'Close',  //Optional
        setLabel: 'Set',  //Optional
        setButtonType: 'button-dark',  //Optional
        todayButtonType: 'button-stable',  //Optional
        closeButtonType: 'button-stable',  //Optional
        inputDate: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),  //Optional
        templateType: 'popup', //Optional
        showTodayButton: 'true', //Optional
        from: new Date(1950, 1, 1), //Optional
        callback: function (val) {  //Mandatory
            if (typeof (val) === 'undefined') {
                console.log('No date selected');
            } else {
                $scope.datePickerObject.inputDate = val;
            }
        },
        dateFormat: 'MMMM d, yyyy', //Optional
        closeOnSelect: false, //Optional
    };

    // methods for sending messages
    //==========================================================================

    $scope.deleteDefect = function(defectNode) {
        Util.loadingStart("Deleting defect...");
        Server.deleteDefect(defectNode).then(function (response) {
            $scope.loadDefects();
        }, function (response) {
            Util.loadingStop();
            Util.showErrorMessage("<b>Could not delete defect.</b> Please check your internet connection.");
        });
    }

    $scope.getStructures = function () {
        return Server.getStructures();
    }

    $scope.getStructureInspections = function(structure) {
        return Server.getStructureInspections(structure);
    }

    $scope.getInspectionInspectors = function (inspection) {
        return Server.getInspectionInspectors(inspection);
    }

    $scope.getDefects = function (inspection, inspector, dt, se, ld, description, date, isDraft, priority) {
        if (!inspection) inspection = $scope.data.filters.inspectionChoice.inspection.value;
        if (!inspector) inspector = $scope.data.filters.inspectorChoice.inspector.value;
        if (!dt) dt = ($scope.data.filters.defectTypeChoice.node) ? $scope.data.filters.defectTypeChoice.node : null;
        if (!se) se = ($scope.data.filters.structuralElementChoice.node) ? $scope.data.filters.structuralElementChoice.node : null;
        if (!ld) ld = ($scope.data.filters.locationDescriptionChoice.node) ? $scope.data.filters.locationDescriptionChoice.node : null;
        if (!description) description = ($scope.data.filters.descriptionChoice) ? $scope.data.filters.descriptionChoice: null;
        if (!date) date = $scope.datePickerObject.inputDate.toISOString();
        if (!isDraft) isDraft = getCompletion($scope.data.filters.completionChoice);
        if (!priority) priority = $scope.data.filters.priorityChoice;
        return Server.getDefects(inspection, inspector, dt, se, ld, description, date, isDraft, priority);
    }

    $scope.getHierarchyElements = function (hierarchy) {
        return Server.getHierarchyElements(hierarchy);
    }

    
    
    // methods for changing state
    //=====================================================================
    $scope.goToAddDefect = function () {
        var structure = $scope.data.filters.structureChoice;
        var dt = structure.dtHier;
        var se = structure.seHier;
        var ld = structure.ldHier;
        
        var params = {
            inspection: $scope.data.filters.inspectionChoice,
            defectTypes: HierarchyElement.createNew(dt.value, "Defect Type", dt.elements),
            structuralElements: HierarchyElement.createNew(se.value, "Structural Element", se.elements),
            locationDescriptions: HierarchyElement.createNew(ld.value, "Location Description", ld.elements)
        }

        $state.go("app.addDefect", params);
    }

    // misc
    //========================================================================

    var getCompletion = function (completion) {
        if (completion === "Drafts") return true;
        if (completion === "Saved") return false;
        if (completion === "Saved and Drafts") return null;
    }

    var formatDate = function (timestamp) {
        var date = new Date(timestamp);
        return date;
    }

    $scope.getTags = function (defect) {
        var tagsString = "";
        if (defect.dt && defect.dtname) tagsString = tagsString + (tagsString.length > 0 ? ", " : "") + defect.dtname.value;
        if (defect.se && defect.sename) tagsString = tagsString + (tagsString.length > 0 ? ", " : "") + defect.sename.value;
        if (defect.ld && defect.ldname) tagsString = tagsString + (tagsString.length > 0 ? ", " : "") + defect.ldname.value;
        if (tagsString.length === 0) tagsString = "None.";
        return tagsString;
    }

    // viewing functions
    //=========================================================================

    $scope.toggleShowSortByChoices = function () {
        $scope.data.filters.showSortByChoices = !$scope.data.filters.showSortByChoices;
        $scope.data.filters.showSortDirectionChoices = false;
        $scope.data.filters.showStructureChoices = false;
        $scope.data.filters.showInspectionChoices = false;
        $scope.data.filters.showInspectorChoices = false;
        $scope.data.filters.showCompletionChoices = false;

        $ionicScrollDelegate.$getByHandle('mainContent').resize();
        if ($scope.data.filters.showDateChoices) {
            $scope.scrollTo("sortByFilter");
        }
    }

    $scope.toggleShowSortDirectionChoices = function () {
        $scope.data.filters.showSortByChoices = false;
        $scope.data.filters.showSortDirectionChoices = !$scope.data.filters.showSortDirectionChoices;
        $scope.data.filters.showStructureChoices = false;
        $scope.data.filters.showInspectionChoices = false;
        $scope.data.filters.showInspectorChoices = false;
        $scope.data.filters.showCompletionChoices = false;

        $ionicScrollDelegate.$getByHandle('mainContent').resize();
        if ($scope.data.filters.showDateChoices) {
            $scope.scrollTo("sortDirectionFilter");
        }
    }

    $scope.toggleShowCompletionChoices = function () {
        $scope.data.filters.showSortByChoices = false;
        $scope.data.filters.showSortDirectionChoices = false;
        $scope.data.filters.showStructureChoices = false;
        $scope.data.filters.showInspectionChoices = false;
        $scope.data.filters.showInspectorChoices = false;
        $scope.data.filters.showCompletionChoices = !$scope.data.filters.showCompletionChoices;

        $ionicScrollDelegate.$getByHandle('mainContent').resize();
        if ($scope.data.filters.showDateChoices) {
            $scope.scrollTo("completionFilter");
        }
    }

    $scope.toggleShowStructureChoices = function () {
        $scope.data.filters.showSortByChoices = false;
        $scope.data.filters.showSortDirectionChoices = false;
        $scope.data.filters.showStructureChoices = !$scope.data.filters.showStructureChoices;
        $scope.data.filters.showInspectionChoices = false;
        $scope.data.filters.showInspectorChoices = false;
        $scope.data.filters.showCompletionChoices = false;

        $ionicScrollDelegate.$getByHandle('mainContent').resize();
        if ($scope.data.filters.showDateChoices) {
            $scope.scrollTo("structureFilter");
        }
    }

    $scope.$watch('data.filters.structureChoice', function () {
        var structure = $scope.data.filters.structureChoice
        if (structure && structure.inspections)
            $scope.data.filters.inspectionChoice = structure.inspections[0];
    });

    $scope.toggleShowInspectionChoices = function () {
        $scope.data.filters.showSortByChoices = false;
        $scope.data.filters.showSortDirectionChoices = false;
        $scope.data.filters.showStructureChoices = false;
        $scope.data.filters.showInspectionChoices = !$scope.data.filters.showInspectionChoices;
        $scope.data.filters.showInspectorChoices = false;
        $scope.data.filters.showCompletionChoices = false;

        $ionicScrollDelegate.$getByHandle('mainContent').resize();
        if ($scope.data.filters.showDateChoices) {
            $scope.scrollTo("inspectionFilter");
        }
    }

    $scope.$watch('data.filters.inspectionChoice', function () {
        var findInspectorInList = function (inspectorList, inspector) {
            var inspectorIndex = -1;
            for (var i = 0; i < inspectorList.length; i++) {
                if (inspector.inspector.value === inspectorList[i].inspector.value) {
                    inspectorIndex = i;
                    break;
                }
            }

            return inspectorIndex;
        }

        var inspection = $scope.data.filters.inspectionChoice

        // if the currently chosen inspector is assigned to the newly chosen inspection, choose him for the inspector filter, otherwise choose the current inspector


        if (inspection && inspection.inspectors) {        
            var inspectorIndex = 0;
            if (inspection.inspectors.length > 1) {
                inspectorIndex = findInspectorInList(inspection.inspectors, $scope.data.filters.inspectorChoice);
                if (inspectorIndex === -1) {
                    inspectorIndex = findInspectorInList(inspection.inspectors, $rootScope.data.user);
                }
            }

            if (inspection.inspectors[inspectorIndex])
                $scope.data.filters.inspectorChoice = inspection.inspectors[inspectorIndex];
        }
    });

    $scope.toggleShowInspectorChoices = function () {
        $scope.data.filters.showSortByChoices = false;
        $scope.data.filters.showSortDirectionChoices = false;
        $scope.data.filters.showStructureChoices = false;
        $scope.data.filters.showInspectionChoices = false;
        $scope.data.filters.showInspectorChoices = !$scope.data.filters.showInspectorChoices;
        $scope.data.filters.showCompletionChoices = false;

        $ionicScrollDelegate.$getByHandle('mainContent').resize();
        if ($scope.data.filters.showDateChoices) {
            $scope.scrollTo("inspectorFilter");
        }
    }

    $scope.toggleShowFilters = function () {
        $ionicScrollDelegate.resize();
        $scope.data.showFilters = !$scope.data.showFilters;
        if (!$scope.data.showFilters) // may want to check if filters have changed
            $scope.loadDefects();
    }

    $scope.scrollTo = function (target) {
        $location.hash(target);   //set the location hash
        var handle = $ionicScrollDelegate.$getByHandle('mainContent');
        handle.anchorScroll(true);  // 'true' for animation
    }

    $scope.showImageModal = function (src) {
        $scope.data.imageModalData.image = src;
        $scope.showModal('templates/modals/imageModal.html');
    }

    $scope.showHierarchyChooserModalDefectType = function () {
        var hierarchy = HierarchyElement.createNew("", "Defect Type", $scope.data.filters.structureChoice.dtHier.elements);

        $scope.data.hierarchyChooserModalData.hierarchy = hierarchy;
        $scope.data.hierarchyChooserModalData.title = "Defect Type";
        $scope.data.hierarchyChooserModalData.callback = function (element) {
            $scope.data.filters.defectTypeChoice = element;
            $scope.closeModal();
        }
        $scope.showModal('templates/modals/hierarchyChooserModal.html');
    }

    $scope.showHierarchyChooserModalStructuralElement = function () {
        var hierarchy = HierarchyElement.createNew("", "Structural Element", $scope.data.filters.structureChoice.seHier.elements);

        $scope.data.hierarchyChooserModalData.hierarchy = hierarchy;
        $scope.data.hierarchyChooserModalData.title = "Structural Element";
        $scope.data.hierarchyChooserModalData.callback = function (element) {
            $scope.data.filters.structuralElementChoice = element;
            $scope.closeModal();
        }
        $scope.showModal('templates/modals/hierarchyChooserModal.html');
    }

    $scope.showHierarchyChooserModalLocationDescription = function () {
        var hierarchy = HierarchyElement.createNew("", "Location Description", $scope.data.filters.structureChoice.ldHier.elements);

        $scope.data.hierarchyChooserModalData.hierarchy = hierarchy;
        $scope.data.hierarchyChooserModalData.title = "Location Description";
        $scope.data.hierarchyChooserModalData.callback = function (element) {
            $scope.data.filters.locationDescriptionChoice = element;
            $scope.closeModal();
        }
        $scope.showModal('templates/modals/hierarchyChooserModal.html');
    }

    $scope.showModal = function (templateUrl) {
        $ionicModal.fromTemplateUrl(templateUrl, {
            scope: $scope,
            animation: "animated fadeInModal"
        }).then(function (modal) {
            $scope.modal = modal;
            $scope.modal.show();
        });
    }

    // Close the modal
    $scope.closeModal = function () {
        if ($scope.modal) {
            $scope.modal.hide();
            $scope.modal.remove();
        }
    };

    $scope.showDeleteConfirmation = function(defectId) {
        // An elaborate, custom popup
        $ionicPopup.show({
            title: 'Delete Defect?',
            scope: $scope,
            buttons: [
              { text: 'Back' },
              {
                  text: '<b>Delete</b>',
                  type: 'button-dark',
                  onTap: function (e) {
                      $scope.deleteDefect(defectId);
                  }
              }
            ]
        });
    }

    $scope.loadFilters = function () {
        if (!$scope.data.filters.filtersLoaded) {
            Util.loadingStart("Loading defects...");

            // temporarily holds the state of the hierarchy
            var filterHierarchy = [];

            // get all structures
            $scope.getStructures().then(function (structures) {
                filterHierarchy = structures;
                var promises = [];
                for (i = 0; i < structures.length; i++) {
                    var structure = structures[i];
                    var node = structure.structure.value;
                    promises.push($scope.getStructureInspections(node));
                }

                return Promise.all(promises);
                // get all inspections of all structures
            }).then(function (inspections) {
                for (i = 0; i < filterHierarchy.length; i++) {
                    if (inspections[i]) {
                        filterHierarchy[i].inspections = inspections[i];
                    }
                }

                var inspectionList = [];
                for (i = 0; i < inspections.length; i++)
                    for (j = 0; j < inspections[i].length; j++)
                        inspectionList.push(inspections[i][j]);

                var promises = [];
                for (i = 0; i < inspectionList.length; i++) {
                    var inspection = inspectionList[i];
                    var node = inspection.inspection.value;
                    promises.push($scope.getInspectionInspectors(node));
                }

                return Promise.all(promises);
                // get all inspectors of all inspections of all structures
            }).then(function (inspectors) {
                var numInspectionsSeen = 0;
                for (i = 0; i < filterHierarchy.length; i++) {
                    for (j = 0; j < filterHierarchy[i].inspections.length; j++) {
                        if (numInspectionsSeen < inspectors.length) {
                            filterHierarchy[i].inspections[j].inspectors = inspectors[numInspectionsSeen];
                        }

                        numInspectionsSeen++;
                    }
                }

                // filter out all structures and inspections that don't have the signed-in inspector assigned to them
                for (i = filterHierarchy.length - 1; i >= 0; i--) {
                    for (j = filterHierarchy[i].inspections.length - 1; j >= 0; j--) {
                        var hasInspector = false;
                        for (k = filterHierarchy[i].inspections[j].inspectors.length - 1; k >= 0; k--) {
                            if (filterHierarchy[i].inspections[j].inspectors[k].inspector.value === $rootScope.data.user.inspector) {
                                hasInspector = true;
                                break;
                            }
                        }
                        if (!hasInspector) {
                            filterHierarchy[i].inspections.splice(j, 1);
                        }
                    }
                    if (filterHierarchy[i].inspections.length === 0) {
                        filterHierarchy.splice(i, 1);
                    }
                }

                var hierarchyList = [];
                for (i = 0; i < filterHierarchy.length; i++) {
                    hierarchyList.push(filterHierarchy[i].seHier);
                    hierarchyList.push(filterHierarchy[i].dtHier);
                    hierarchyList.push(filterHierarchy[i].ldHier);
                }

                var promises = [];
                for (i = 0; i < hierarchyList.length; i++) {
                    var hierarchy = hierarchyList[i];
                    var node = hierarchy.value;
                    promises.push($scope.getHierarchyElements(node));
                }

                return Promise.all(promises);
            }).then(function (elements) {
                var numInspectionsSeen = 0;
                for (i = 0; i < filterHierarchy.length; i++) {
                    var elementIndex = i * 3;
                    filterHierarchy[i].seHier.elements = elements[elementIndex];
                    filterHierarchy[i].dtHier.elements = elements[elementIndex + 1];
                    filterHierarchy[i].ldHier.elements = elements[elementIndex + 2];
                }

                $scope.data.filters.filterHierarchy = filterHierarchy;
                if (filterHierarchy.length > 0) {
                    $scope.data.filters.structureChoice = filterHierarchy[0];
                    $scope.data.filters.inspectionChoice = filterHierarchy[0].inspections[0];
                    $scope.data.filters.inspectorChoice = filterHierarchy[0].inspections[0].inspectors[0];
                }

                $scope.data.filters.filtersLoaded = true;

                $scope.loadDefects();
            });
        }
    }

    $scope.loadDefects = function () {
        if ($scope.data.filters.filtersLoaded) {
            Util.loadingStart("Loading defects...");
            var loadedDefects = [];
            $scope.getDefects().then(function (defects) {
                loadedDefects = defects;
                return Geolocation.getCurrentPosition();
            }).then(function (position) {
                // format defects
                var latitude = position.coords.latitude;
                var longitude = position.coords.longitude;

                for (var i = 0; i < loadedDefects.length; i++) {
                    var defect = loadedDefects[i];
                    if (defect.datetime) {
                        defect.datetime.value = new Date(defect.datetime.value);
                    }

                    var distance = Distance.distanceToDefectInMiles(defect, latitude, longitude);
                    defect.distance = {};
                    defect.distance.value = distance;
                }

                // apply leftover filters and sorting
                loadedDefects = $scope.data.filters.distanceChoice > 0 ? Distance.getDefectsWithinDistance(loadedDefects, $scope.data.filters.distanceChoice) : loadedDefects;
                loadedDefects = Sorting.sort(loadedDefects, $scope.data.filters.sortByChoice);

                var urls = [];
                for (var i = 0; i < $scope.data.defects.length; i++) {
                    if ($scope.data.defects[i].image) {
                        var url = $scope.data.serverData.imageAddress + $scope.data.defects[i].image.value;
                        urls.push(url);
                    }
                }

                // preload all images
                return ImagePreloader.Cache(urls);
            }).then(function () {
                $scope.data.defects = loadedDefects;
                Util.loadingStop();
            });
        }
    }

    // defect options popover
    //=========================================================================
    $scope.initDefectOptionsPopover = function () {
        $ionicPopover.fromTemplateUrl('templates/popovers/defectOptionsPopover.html', {
            scope: $scope
        }).then(function (popover) {
            $scope.defectOptionsPopover = popover;
        });

        $scope.openDefectOptionsPopover = function ($event, defectNode) {
            $scope.data.defectPopoverData.deletedDefectNode = defectNode;
            $scope.defectOptionsPopover.show($event);
        };

        $scope.closeDefectOptionsPopover = function () {
            $scope.defectOptionsPopover.hide();
        };

        $scope.$on('$destroy', function () {
            $scope.defectOptionsPopover.remove();
        });
    }

    // init
    //=========================================================================

    $scope.$on('$ionicView.loaded', function () {
        $scope.initDefectOptionsPopover();
        $scope.loadFilters();
    });

    $scope.$on('$ionicView.enter', function () {
        $scope.loadDefects();
    });


});

//=====================================================================================================================
// ADD DEFECT CONTROLLER
//=====================================================================================================================

app.controller("AddDefectCtrl", function ($scope,
                                            $rootScope,
                                            $state,
                                            $stateParams,
                                            $ionicSlideBoxDelegate,
                                            $ionicPopup,
                                            $ionicModal,
                                            $window,
                                            ImageService,
                                            Geolocation,
                                            Orientation,
                                            Motion,
                                            Server,
                                            Priority,
                                            Util) {

    $scope.slideBox = $ionicSlideBoxDelegate.$getByHandle('AddDefectSlideBox');
    $scope.priority = Priority;
    $scope.data = {
        inspection: $stateParams.inspection,
        defectDescription: "",
        defectPriority: Priority.medium(),
        defectTypes: $stateParams.defectTypes,
        structuralElements: $stateParams.structuralElements,
        locationDescriptions: $stateParams.locationDescriptions,
        chosenDT: null,
        chosenSE: null,
        chosenLD: null,
        photoURI: 'img/Transparent_200x200.jpg',
        photoBase64: null,
        xAcceleration: null,
        yAcceleration: null,
        zAcceleration: null,
        heading: null,
        latitude: null,
        longitude: null,
        altitude: null,
        imageModalData: {
            image: null
        }
    }

    // loading functions
    //====================================================================

    // methods for changing state
    //=====================================================================
    $scope.goToDefects = function () {
        $state.go("app.defects");
    }


    // server functions
    //=====================================================================

    $scope.addDefect = function(defect) {
        Server.addDefect(defect)
            .then(function (response) {
                Util.loadingStop();
                $scope.goToDefects();
            }, function (response) {
                Util.loadingStop();
                Util.showErrorMessage("<b>Could not save defect.</b> Please check your internet connection.");
            });
    }

    $scope.getDataAndAddDefect = function (isDraft) {
        if ($stateParams.inspection) {
            Util.loadingStart("Saving defect...");

            var defect = {
                inspection: $stateParams.inspection.inspection.value,
                inspector: $rootScope.data.user.inspector,
                description: $scope.data.defectDescription,
                date: new Date().toISOString(),
                priority: $scope.data.defectPriority.toString(),
                isDraft: isDraft ? "true" : "false",
                image: $scope.data.photoBase64
            };
            if ($scope.data.chosenDT) defect.defectType = $scope.data.chosenDT.node;
            if ($scope.data.chosenSE) defect.structuralElement = $scope.data.chosenSE.node;
            if ($scope.data.chosenLD) defect.locationDescription = $scope.data.chosenLD.node;
            if ($scope.data.xAcceleration) defect.x = $scope.data.xAcceleration.toString();
            if ($scope.data.yAcceleration) defect.y = $scope.data.yAcceleration.toString();
            if ($scope.data.zAcceleration) defect.z = $scope.data.zAcceleration.toString();
            if ($scope.data.heading) defect.heading = $scope.data.heading.toString();
            if ($scope.data.latitude) defect.latitude = $scope.data.latitude.toString();
            if ($scope.data.longitude) defect.longitude = $scope.data.longitude.toString();
            if ($scope.data.altitude) defect.altitude = $scope.data.altitude.toString();

            $scope.addDefect(defect);
        }
    }


    // pictures
    //=========================================================================

    $scope.takePicture = function () {
        ImageService.takePicture().then(function (image) {

            Motion.getCurrentAcceleration().then(function (acceleration) {
                $scope.data.xAcceleration = acceleration.x;
                $scope.data.yAcceleration = acceleration.y;
                $scope.data.zAcceleration = acceleration.z;
            });

            Orientation.getCurrentHeading().then(function (heading) {
                $scope.data.heading = heading.magneticHeading;
            });

            Geolocation.getCurrentPosition().then(function (position) {
                $scope.data.latitude = position.coords.latitude;
                $scope.data.longitude = position.coords.longitude;
                $scope.data.altitude = position.coords.altitude;
            });

            $scope.data.photoURI = image.src;
            ImageService.convertSavedImageToData($scope.data.photoURI, function (base64String) {
                $scope.data.photoBase64 = base64String;
            });

            var windowWidth = $window.innerWidth;
            var windowHeight = $window.innerHeight;
            var rect = document.getElementById("photoContent").getBoundingClientRect();

            var heightRatio = ((rect.height - 50) * 1.0) / image.height;
            var width = image.width * heightRatio;
            if (width > rect.width) width = rect.width;

            document.getElementById("photo").style.width = width + "px";
            document.getElementsByClassName("photoContainer")[0].style.height = (rect.height - 30) + "px";
        }, function (err) {
            Util.showErrorMessage("<b>Could not take photo.</b>");
        });
    }


    // misc
    //=========================================================================

    $scope.disableSwipe = function () {
        $scope.slideBox.enableSlide(false);
    }

    $scope.slideTo = function (index) {
        $scope.slideBox.slide(index);
    };

    $scope.isOnSlide = function (index) {
        return index == $scope.slideBox.currentIndex();
    }

    $scope.showCreateConfirmation = function () {
        // An elaborate, custom popup
        $ionicPopup.show({
            title: 'Save Defect?',
            scope: $scope,
            buttons: [
              { text: 'Back' },
              {
                  text: 'Draft',
                  onTap: function (e) {
                      $scope.getDataAndAddDefect(true);
                  }
              },
              {
                  text: '<b>Save</b>',
                  type: 'button-dark',
                  onTap: function (e) {
                      $scope.getDataAndAddDefect(false);
                  }
              }
            ]
        });
    }

    $scope.photosComplete = function () {
        return $scope.data.images.length > 0;
    }
    $scope.descriptionComplete = function () {
        return !($scope.data.defectDescription === '');
    }
    $scope.DTComplete = function () {
        return !($scope.data.chosenDT.id === null);
    }
    $scope.SEComplete = function () {
        return !($scope.data.chosenSE.id === null);
    }
    $scope.LDComplete = function () {
        return !($scope.data.chosenLD.id === null);
    }
    $scope.defectReportComplete = function () {
        return $scope.photosComplete() && $scope.descriptionComplete() && $scope.DTComplete() && $scope.SEComplete() && $scope.LDComplete();
    }

    $scope.showImageModal = function (src) {
        $scope.data.imageModalData.image = src;
        $scope.showModal('templates/modals/imageModal.html');
    }

    $scope.showModal = function (templateUrl) {
        $ionicModal.fromTemplateUrl(templateUrl, {
            scope: $scope,
            animation: "animated fadeInModal"
        }).then(function (modal) {
            $scope.modal = modal;
            $scope.modal.show();
        });
    }

    // Close the modal
    $scope.closeModal = function () {
        $scope.modal.hide();
        $scope.modal.remove()
    };

    // init
    //=========================================================================

    $scope.$on('$ionicView.enter', function () {
        //$scope.takePicture();
    });

});

app.controller("SettingsCtrl", function ($scope) {

});
