var app = angular.module('SIT.services', [])

/*
Factory used to standardize the creation of objects identical to the objects returned by Server.
*/
app.factory('HierarchyElement', function () {
    return {

        createNew: function (node, elementName, children) {
            return {
                node: node,
                elementName: elementName,
                children: children
            };
        },

        createAny: function () {
            return {
                node: "",
                elementName: "Any",
                children: []
            };
        }

    }
});

app.factory('Inspector', function () {
    return {

        createNew: function (first, last, inspector) {
            return {
                first: first,
                last: last,
                inspector: inspector
            };
        },

        createAny: function () {
            return {
                first: "Any",
                last: "",
                inspector: ""
            };
        }

    }
});

app.factory('Priority', function () {

    var VERY_LOW = 0;
    var LOW = 1;
    var MEDIUM = 2;
    var HIGH = 3;
    var VERY_HIGH = 4;

    return {

        veryLow: function() {
            return VERY_LOW;
        },

        low: function() {
            return LOW;
        },

        medium: function () {
            return MEDIUM;
        },

        high: function () {
            return HIGH;
        },

        veryHigh: function () {
            return VERY_HIGH;
        },

        getPriorityString: function (priority) {
            var priNum = null;
            if (typeof priority === 'number')
                priNum = priority;
            if (typeof priority === 'string')
                priNum = parseInt(priority);

            if (typeof priNum === "number") {
                if (priNum === VERY_LOW) return "Very Low";
                if (priNum === LOW) return "Low";
                if (priNum === MEDIUM) return "Medium";
                if (priNum === HIGH) return "High";
                if (priNum === VERY_HIGH) return "Very High";
            }
            else
                return "ERROR";
        },

        getPriorityBackgroundColor: function (priority) {
            var priNum = null;
            if (typeof priority === 'number')
                priNum = priority;
            if (typeof priority === 'string')
                priNum = parseInt(priority);

            if (typeof priNum === "number") {
                if (priNum === VERY_LOW) return "priorityVeryLow";
                if (priNum === LOW) return "priorityLow";
                if (priNum === MEDIUM) return "priorityMedium";
                if (priNum === HIGH) return "priorityHigh";
                if (priNum === VERY_HIGH) return "priorityVeryHigh";
            }
            else
                return "ERROR";
        }

    }
});

app.factory('Sorting', function () {

    var FIELDS = {
        INSPECTOR: {
            NAME: "Inspector",
            COMPARE: function(a, b) {
                return a.inspectorLastName.value.localeCompare(b.inspectorLastName.value);
            }
        },
        DATE: {
            NAME: "Date",
            COMPARE: function (a, b) {
                if (a.datetime.value < b.datetime.value)
                    return -1;
                else if (a.datetime.value > b.datetime.value)
                    return 1;
                else
                    return 0;
            }
        },
        DISTANCE: {
            NAME: "Distance",
            COMPARE: function (a, b) {
                if (a.distance.value < b.distance.value)
                    return -1;
                else if (a.distance.value > b.distance.value)
                    return 1;
                else
                    return 0;
            }
        },
        PRIORITY: {
            NAME: "Priority",
            COMPARE: function (a, b) {
                if (parseInt(a.priority.value) < parseInt(b.priority.value))
                    return -1;
                else if (parseInt(a.priority.value) > parseInt(b.priority.value))
                    return 1;
                else
                    return 0;
            }
        }
    }

    return {

        Inspector: function () {
            return FIELDS.INSPECTOR;
        },

        Date: function () {
            return FIELDS.DATE;
        },

        Distance: function () {
            return FIELDS.DISTANCE;
        },

        Priority: function () {
            return FIELDS.PRIORITY;
        },

        sort: function (defects, field) {
            return defects.sort(field.COMPARE);
        },

    }
});

app.factory('Distance', function (Geolocation) {

    var MILES_PER_KM = 0.621371;

    function getDistanceFromLayLonInMiles(lat1, lon1, lat2, lon2) {
        return getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) / MILES_PER_KM;
    }

    function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2 - lat1);  // deg2rad below
        var dLon = deg2rad(lon2 - lon1);
        var a =
          Math.sin(dLat / 2) * Math.sin(dLat / 2) +
          Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
          Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return d;
    }

    function deg2rad(deg) {
        return deg * (Math.PI / 180)
    }

    return {

        distanceToDefectInMiles: function (defect, lat, long) {
            var defectLatitude = parseFloat(defect.latitude.value);
            var defectLongitude = parseFloat(defect.longitude.value);
            return getDistanceFromLayLonInMiles(lat, long, defectLatitude, defectLongitude);
        },

        distanceToDefectInKm: function (defect, lat, long) {
            var defectLatitude = parseFloat(defect.latitude.value);
            var defectLongitude = parseFloat(defect.longitude.value);
            return getDistanceFromLayLonInKm(lat, long, defectLatitude, defectLongitude);
        },

        getDefectsWithinDistance: function (defects, distance) {
            var filteredResults = [];

            if (Array.isArray(defects)) {
                if (typeof distance === 'number') {
                    var latitude = 0;
                    var longitude = 0;
                    Geolocation.getCurrentPosition().then(function (position) {
                        latitude = position.coords.latitude;
                        longitude = position.coords.longitude;
                        for (var i = 0; i < defects.length; i++) {
                            var defect = defects[i];
                            var defectLatitude = parseFloat(defect.latitude.value);
                            var defectLongitude = parseFloat(defect.longitude.value);
                            var calcDistance = defect.distance ? defect.distance.value : getDistanceFromLatLonInKm(latitude, longitude, defectLatitude, defectLongitude);
                            if (calcDistance <= distance) {
                                filteredResults.push(defect);
                            }
                        }
                    });
                }
                else {
                    filteredResults = defects;
                }
            }

            return filteredResults;
        }

    }
});

/*
Factory used to interface with the SIT server.
*/
app.factory('Server', function (    $http, // used to send messages to the REST API
                                    $state, // used to change the view of the application
                                    $ionicLoading, // used to manipulate loading screen
                                    $timeout, // used to make actions wait for periods of time
                                    $rootScope, // used to set global information, like the user
                                    HierarchyElement,
                                    Util)
{
    var address = "http://54.175.40.59:5498";
    var imageAddress = "https://s3.amazonaws.com/sit-defects/";

    var formatBNode = function (node) {
        var formatted = node;
        if (node.substr(0, 1) !== "_:")
            formatted = "_:" + node;
        return formatted;
    }

    var noParse = function (response) {
        return response.data;
    }

    var parseInfoOnOneLine = function (response) {
        return response.data.results ? response.data.results.bindings : [];
    }

    var parseHierarchyElements = function (response) {
        // get line-by-line data
        var results = response.data.results ? response.data.results.bindings : [];

        // determine hierarchy root node
        var root = "";
        var nodes = [];
        var elementNames = [];
        var parentNodes = [];
        for (var i = 0; i < results.length; i++) {
            nodes.push(results[i].element.value);
            elementNames.push(results[i].elementName.value);
            parentNodes.push(results[i].parent.value);
        }

        for (var i = 0; i < parentNodes.length; i++) {
            if (nodes.indexOf(parentNodes[i]) === -1) {
                root = parentNodes[i];
                break;
            }
        }

        // create a hash map for each element that holds its children elements
        var map = new Map();
        for (var i = 0; i < results.length; i++) {
            map.set(results[i].element.value, []);
        }

        for (var i = 0; i < results.length; i++) {
            var child = results[i].element.value;
            var parent = results[i].parent.value;
            // top level elements will have the hierarchy root as a parent, but the root should not be considered when building the hierarchy
            if (parent !== root)
                map.get(parent).push(child);
        }

        // function that takes an element, finds the children of the element, then builds a list of children of the element
        // recursively calls itself on the children to build the hierarchy of elements
        // when there are no children, an empty list is returned.
        var buildHierarchy = function (node) {
            var elementName = elementNames[nodes.indexOf(node)];
            var childrenElements = map.get(node);
            var children = [];

            for (var i = 0; i < childrenElements.length; i++)
                children.push(buildHierarchy(childrenElements[i]));

            var parent = HierarchyElement.createNew(node, elementName, children);
            return parent;
        }

        var parsedResult = [];
        var keys = map.keys();
        var next = keys.next();
        while(!next.done) {
            var node = next.value;
            var parent = parentNodes[nodes.indexOf(node)];
            if (parent === root)
                parsedResult.push(buildHierarchy(node));

            next = keys.next();
        }

        return parsedResult;
    }

    var query = function (url, data, onParse) {
        if (url && onParse) {
            // default data is empty
            if (!data)
                data = {};

            return new Promise(function(resolve, reject) {
                // default success function returns the response
                var success = function (response) {
                    data = onParse(response);
                    resolve(data);
                };

                // default failure function returns the response (which will probably be an error)
                var failure = function (response) {
                    reject(response);
                };

                // make sure results are sent as json (flag in the query)
                data.json = "true";
                if ($rootScope.data.sessionKey) data.user = $rootScope.data.sessionKey;

                // construct request
                var urlString = address + url;
                var request = $http({
                    method: "POST",
                    url: urlString,
                    data: data
                });

                // call request followed by the success or failure function when response is recieved
                request.then(success, failure);
            });
        }

        return null;
    }

    return {

        /* Attempts to login using a username and password
            SENDS: username string, password string
            RECIEVES: user information */
        login: function (username, password) {
            var url = "/login";
            var data = {
                username: username,
                password: password
            };

            return query(url, data, noParse);
        },

        /* Attempts to retrieve all structures
        SENDS: None
        RECIEVES: All structures */
        getStructures: function() {
            var url = "/structure/all";
            var data = {};

            return query(url, data, parseInfoOnOneLine);
        },

        /* Attempts to retrieve all inspections
        SENDS: None
        RECIEVES: All inspections */
        getInspections: function () {
            var url = "/inspection/all";
            var data = {};

            return query(url, data, parseInfoOnOneLine);
        },

        /* Attempts to retrieve all inspections related to a specific structure
            SENDS: structure
            RECIEVES: All inspections of structure */
        getStructureInspections: function (structure) {
            structure = formatBNode(structure);
            var url = "/structure/inspections";
            var data = {
                structure: structure
            };

            return query(url, data, parseInfoOnOneLine);
        },


        /* Attempts to retrieve all inspections related to a specific inspector
            SENDS: inspector
            RECIEVES: All inspections of inspector */
        getInspectorInspections: function (inspector) {
            inspector = formatBNode(inspector);
            var url = "/inspector/inspections";
            var data = {
                inspector: inspector
            };

            return query(url, data, parseInfoOnOneLine);
        },


        /* Attempts to retrieve all inspectors related to a specific inspection
            SENDS: inspection
            RECIEVES: All inspectors of inspection */
        getInspectionInspectors: function (inspection) {
            inspection = formatBNode(inspection);
            var url = "/inspection/inspectors";
            var data = {
                inspection: inspection
            };

            return query(url, data, parseInfoOnOneLine);
        },

        /* Attempts to retrieve all descendant hierarchy elements of the given hierarchy or hierarchy element
            SENDS: hierarchy or hierarchy element
            RECIEVES: All decendants of the hierarchy or hierarchy element*/
        getHierarchyElements: function (hierarchy) {
            hierarchy = formatBNode(hierarchy);
            var url = "/hierarchy/elements";
            var data = {
                hierarchy: hierarchy
            };

            return query(url, data, parseHierarchyElements);
        },

        /* Attempts to retrieve all hierarchies of a given structure
           SENDS: structure
           RECIEVES: Hierarchy roots of the structure*/
        getStructureHierarchies: function (structure) {
            structure = formatBNode(structure);
            var url = "/structure/hierarchies";
            var data = {
                structure: structure
            };

            return query(url, data, parseInfoOnOneLine);
        },

        /* Attempts to retrieve all hierarchies of a given structure
           SENDS: structure
           RECIEVES: Hierarchy roots of the structure*/
        getDefects: function (inspection, inspector, dt, se, ld, description, datetime, isDraft, priority) {
            var url = "/defect/search";
            var data = {};
            if (inspection) data.inspection = formatBNode(inspection);
            if (inspector) data.inspector = formatBNode(inspector);
            if (dt) data.defectType = formatBNode(dt);
            if (se) data.structuralElement = formatBNode(se);
            if (ld) data.locationDescription = formatBNode(ld);
            if (description) data.description = description;
            if (datetime) data.datetime = datetime;
            if (typeof(isDraft) === "boolean") data.isDraft = isDraft;
            if (priority) data.priority = priority;

            return query(url, data, parseInfoOnOneLine);
        },

        /* Attempts to delete the given defect
           SENDS: defect
           RECIEVES: none*/
        deleteDefect: function (defect) {
            var url = "/defect/delete";
            var data = {};
            data.defect = formatBNode(defect);

            return query(url, data, parseInfoOnOneLine);
        },

        /* Attempts to create a new defect */
        addDefect: function (defect) {
            var url = "/defect/insert";
            defect.inspection = formatBNode(defect.inspection);
            defect.inspector = formatBNode(defect.inspector);
            if (defect.defectType) defect.defectType = formatBNode(defect.defectType);
            if (defect.structuralElement) defect.structuralElement = formatBNode(defect.structuralElement);
            if (defect.locationDescription) defect.locationDescription = formatBNode(defect.locationDescription);

            return query(url, defect, parseInfoOnOneLine);
        },

        getImageAddress: function() {
            return imageAddress;
        }
    }
});

app.factory('Util', function (  $ionicPopup,
                                $ionicLoading) {
    return {

        showErrorMessage: function (message, then) {
            if (!message) message = "The application has encountered an error. Please check your network connectivity. If your connection is good, restart the application.";
            if (!then) then = function () { };

            var errorPopup = $ionicPopup.alert({
                template: message,
                title: 'Error',
                okType: "button-dark"
            });

            errorPopup.then(then());
        },

        loadingStart: function (message, delay) {
            message = message || "Loading...";
            delay = delay || 500;
            $ionicLoading.show({
                template: message,
                delay: delay
            });
        },

        loadingStop: function () {
            $ionicLoading.hide();
        }

    }
});

// https://devdactic.com/complete-image-guide-ionic/

app.factory('FileService', function ($window, Util) {
    var images = getImages();
    var IMAGE_STORAGE_KEY = 'images';

    function getImages() {
        var img = $window.localStorage.getItem(IMAGE_STORAGE_KEY);
        if (img) {
            images = JSON.parse(img);
        } else {
            images = [];
        }
        return images;
    };

    function addImage(img) {
        images.push(img);
        $window.localStorage.setItem(IMAGE_STORAGE_KEY, JSON.stringify(images));
    };

    return {
        storeImage: addImage,
        images: getImages
    }
});

app.factory('ImageService', function ($cordovaCamera, $cordovaFile, FileService) {

    function makeid() {
        var text = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        for (var i = 0; i < 10; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    };

    function getOptions() {
        return {
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: false,
            encodingType: Camera.EncodingType.JPEG,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };
    }

    function takePicture() {
        var promise = new Promise(function (resolve, reject) {
            var options = getOptions();

            $cordovaCamera.getPicture(options).then(function (imageUrl) {
                var name = imageUrl.substr(imageUrl.lastIndexOf('/') + 1);
                var namePath = imageUrl.substr(0, imageUrl.lastIndexOf('/') + 1);
                var newName = makeid() + name;
                $cordovaFile.copyFile(namePath, name, cordova.file.dataDirectory, newName)
                  .then(function (info) {
                      FileService.storeImage(info.name);

                      function getWidthAndHeight() {
                          var image = {
                              src: this.src,
                              width: this.width,
                              height: this.height
                          }

                          resolve(image);
                      }

                      function loadFailure() {
                          reject();
                      }

                      var myImage = new Image();
                      myImage.onload = getWidthAndHeight;
                      myImage.onerror = loadFailure;
                      myImage.src = cordova.file.dataDirectory + info.name;
                  }, function (e) {
                      reject();
                  });
            });
        });

        return promise;
    }

    //http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
    function b64toBinaryArray(b64Data) {
        sliceSize = 512;

        var byteCharacters = atob(b64Data);
        var byteArray = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var sliceArray = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                sliceArray[i] = slice.charCodeAt(i);
            }

            Array.prototype.push.apply(byteArray, sliceArray);
        }

        return byteArray;
    }

    //http://stackoverflow.com/questions/6150289/how-to-convert-image-into-base64-string-using-javascript
    function convertFileToDataURLviaFileReader(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.responseType = 'blob';
        xhr.onload = function () {
            var reader = new FileReader();
            reader.onloadend = function () {
                var result = reader.result;
                // gets rid of header information
                var b64 = result.slice(result.indexOf(",") + 1);
                callback(b64);
            }
            reader.readAsDataURL(xhr.response);
        };
        xhr.open('GET', url);
        xhr.send();
    }

    return {
        takePicture: takePicture,
        convertSavedImageToData: convertFileToDataURLviaFileReader
    }
});

app.factory('ImagePreloader', [function () {
    return {
        Cache: function (urls) {
            var promises = [];

            for (var i = 0; i < urls.length; i++) {
                var url = urls[i];
                var loadImage = new Promise(function (resolve, reject) {
                    var img = new Image();

                    img.onload = function () {
                        resolve();
                    };

                    img.onerror = function () {
                        reject();
                    };

                    img.src = url;
                });
                promises.push(loadImage);
            }

            return promises;
        }
    }
}]);

app.factory('Geolocation', function ($cordovaGeolocation) {

    var mostRecentPosition = null;
    var watchID = null;
    var defaultOptions = {
        timeout: 5000,
        maximumAge: 3000,
        enableHighAccuracy: false
    };

    function getCurrentPosition() {
        return $cordovaGeolocation.getCurrentPosition(defaultOptions);
    }

    function setWatchPosition(watch) {
        if (watch === true) {
            watchID = $cordovaGeolocation.watchPosition(defaultOptions);
            watchID.then(function (position) {
                mostRecentPosition = position;
            }, function (err) {
                // nothing
            });
        }
        else {
            watchID.clearWatch();
        }
    }

    function getMostRecentPosition() {
        return mostRecentPosition;
    }

    function setMostRecentPosition(position) {
        mostRecentPosition = position;
    }

    return {
        getCurrentPosition: getCurrentPosition,
        setWatchPosition: setWatchPosition,
        getMostRecentPosition: getMostRecentPosition,
        setMostRecentPosition: setMostRecentPosition
    }
});

app.factory('Orientation', function ($cordovaDeviceOrientation) {

    function getCurrentHeading() {
        return $cordovaDeviceOrientation.getCurrentHeading();
    }

    return {
        getCurrentHeading: getCurrentHeading
    }
});

app.factory('Motion', function ($cordovaDeviceMotion) {

    function getCurrentAcceleration() {
        return $cordovaDeviceMotion.getCurrentAcceleration();
    }

    return {
        getCurrentAcceleration: getCurrentAcceleration
    }
});