# README #
### What is SIT? ###

The Structural Inspection Tool, or SIT, aims to modernize the bridge inspection process and eliminate the need for paper-based solutions. SIT is a team of four, all Drexel University Students developing SIT for the Senior Design 2015-2016 class. 

This specific repository deals with the phone implementation, serving as the primary application for daily use. With this application, inspectors can take detailed photos and reports to be committed to the server.

### How do I get set up? ###

## Dependencies ##
### Who do I talk to? ###

* Primary: Jeff Ulman <jeffu92@gmail.com>
* Ryan Snider <rds74@drexel.edu>